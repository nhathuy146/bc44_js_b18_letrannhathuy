// input
var arrayNumber = [];
var nhapSo = function () {
  var numberEl = document.querySelector("#number");
  arrayNumber.push(numberEl.value * 1);
  //   Reset khi nhập number
  numberEl.value = "";
  document.querySelector(".array_number").innerHTML = `<p>${arrayNumber}</p>`;
};
var result = document.querySelector("#result");

// Xử lý đề 1
function tongCacSoDuong() {
  var tongSoDuong = 0;
  for (var i = 0; i < arrayNumber.length; i++) {
    if (arrayNumber[i] >= 0) {
      tongSoDuong += arrayNumber[i];
      // console.log(`🚀 ~ tongCacSoDuong ~ tongCacSoDuong:`, tongSoDuong);
    }
  }
  // output
  result.innerHTML = `<p class="text-white">Tổng các số dương: ${tongSoDuong}</p>`;
}

// Xử lý đề 2
function demSoDuong() {
  var demSoDuong = 0;
  for (var i = 0; i < arrayNumber.length; i++) {
    if (arrayNumber[i] >= 0) {
      demSoDuong++;
    }
  }
  // output
  result.innerHTML = `<p class="text-white">Có: ${demSoDuong} số dương</p>`;
}

// Xử lý đề 3
function timSoNhoNhat() {
  var soNhoNhat = arrayNumber[0];
  for (var i = 1; i < arrayNumber.length; i++) {
    if (soNhoNhat > arrayNumber[i]) {
      soNhoNhat = arrayNumber[i];
    }
  }
  // output
  result.innerHTML = `<p class="text-white">Số nhỏ nhất: ${soNhoNhat}</p>`;
}

// Xử lý đề 4
function timSoDuongNhoNhat() {
  var soDuongNhoNhat = arrayNumber[0];
  for (var i = 1; i < arrayNumber.length; i++) {
    if (soDuongNhoNhat > arrayNumber[i] && arrayNumber[i] >= 0) {
      soDuongNhoNhat = arrayNumber[i];
    }
  }
  // output
  result.innerHTML = `<p class="text-white">Số dương nhỏ nhất: ${soDuongNhoNhat}</p>`;
}

// Xử lý đề 5
function timSoChanCuoiCung() {
  var soChanCuoiCung = -1;
  for (var i = 0; i < arrayNumber.length; i++) {
    if (arrayNumber[i] % 2 == 0) {
      soChanCuoiCung = arrayNumber[i];
    }
  }
  result.innerHTML = `<p class="text-white">Số chẵn cuối cùng: ${soChanCuoiCung}</p>`;
}

// Xử lý đề 6
function doiViTri() {
  var indexSoDauTien = document.querySelector("#soDauTien").value;
  var indexSoThuHai = document.querySelector("#soThuHai").value;
  var temp = arrayNumber[indexSoDauTien];
  arrayNumber[indexSoDauTien] = arrayNumber[indexSoThuHai];
  arrayNumber[indexSoThuHai] = temp;
  // output
  result.innerHTML = `<p class="text-white">Mảng sau khi đổi vị trí: ${arrayNumber}</p>`;
}

// Xử lý đề 7
function sapXep() {
  arrayNumber.sort();

  // output
  result.innerHTML = `<p class="text-white">Mảng đã sắp xếp: ${arrayNumber}</p>`;
}

// Xử lý đề 8
function soNguyenToDauTien() {
  var soNguyenTo = -1;
  for (var i = 0; i < arrayNumber.length; i++) {
    var laSoNguyenTo = true;
    if (arrayNumber[i] > 1) {
      for (var j = 2; j < arrayNumber[i].length; j++) {
        if (arrayNumber[i] % j == 0) {
          laSoNguyenTo = false;
          break;
        }
      }
      if (laSoNguyenTo) {
        soNguyenTo = arrayNumber[i];
        break;
      }
    }
  }
  // output
  result.innerHTML = `<p class="text-white">Số nguyên tố đầu tiên: ${soNguyenTo}</p>`;
}

// Xử lý đề 9
// Nhập mảng số thực
var mangSoThuc = [];
var nhapSoThuc = function () {
  var mangSoThucEl = document.querySelector("#so-thuc");
  mangSoThuc.push(mangSoThucEl.value * 1);
  //   Reset khi nhập số
  mangSoThucEl.value = " ";
  document.querySelector(".so_thuc").innerHTML = `<p>${mangSoThuc}</p>`;
};
// Xử lý yêu cầu
function demSoNguyen() {
  var demSoNguyen = 0;
  for (var i = 0; i < mangSoThuc.length; i++) {
    if (Number.isInteger(mangSoThuc[i]) == true) {
      demSoNguyen++;
    }
  }
  // output
  result.innerHTML = `<p class="text-white">Số nguyên: ${demSoNguyen}</p>`;
}

// Xử lý đê 10
function demVaSoSanh() {
  var soDuong = 0;
  var soAm = 0;
  for (var i = 0; i < arrayNumber.length; i++) {
    if (arrayNumber[i] > 0) {
      soDuong++;
    } else {
      soAm++;
    }
  }
  // output
  if (soDuong > soAm) {
    result.innerHTML = `<p class="text-white">Số dương: ${soDuong} > Số Âm: ${soAm}</p>`;
  } else if (soDuong < soAm) {
    result.innerHTML = `<p>Số dương: ${soDuong} < Số Âm: ${soAm}</p>`;
  } else {
    result.innerHTML = `<p>Số dương: ${soDuong} = Số Âm: ${soAm}</p>`;
  }
}

// output
var myButton = document.querySelector("#my-button");
var selectFunction = document.querySelector("#chuc-nang");

selectFunction.addEventListener("change", function () {
  switch (selectFunction.value) {
    case "1":
      myButton.onclick = function () {
        tongCacSoDuong();
      };
      break;
    case "2":
      myButton.onclick = function () {
        demSoDuong();
      };
      break;
    case "3":
      myButton.onclick = function () {
        timSoNhoNhat();
      };
      break;
    case "4":
      myButton.onclick = function () {
        timSoDuongNhoNhat();
      };
      break;
    case "5":
      myButton.onclick = function () {
        timSoChanCuoiCung();
      };
      break;
    case "6":
      myButton.onclick = function () {
        doiViTri();
      };
      break;
    case "7":
      myButton.onclick = function () {
        sapXep();
      };
      break;
    case "8":
      myButton.onclick = function () {
        soNguyenToDauTien();
      };
      break;
    case "9":
      myButton.onclick = function () {
        demSoNguyen();
      };
      break;
    case "10":
      myButton.onclick = function () {
        demVaSoSanh();
      };
      break;
  }

  // Hiện giao diện khi chọn chức năng
  // Bài 6
  var doiVitri = document.querySelector(".doi-vi-tri").classList;
  if (selectFunction.value == 6) {
    doiVitri.remove("d-none");
  } else {
    doiVitri.add("d-none");
  }

  //  Bài 9
  var sothuc = document.querySelector("#soTThuc").classList;
  if (selectFunction.value == 9) {
    sothuc.remove("d-none");
  } else {
    sothuc.add("d-none");
  }
});
